<?php
require_once '/library/phpQuery.php';
class getContent {
	public $url = null;
	public $typeAttribute = null;
	public $valueAttribute = null;
	public $find = null;
 	public function setValues ($url, $typeAttribute, $valueAttribute, $find) {
 		$this->url = $url;
 		$this->typeAttribute = $typeAttribute;
 		$this->valueAttribute = $valueAttribute;
 		$this->find = $find;
 	}
 	public function curl_content() {
		$ch = curl_init($this->url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; WinNT; en; rv:1.0.2) Gecko/20030311 Beonex/0.8.2-stable');
		//curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiefiles);
		//curl_setopt($ch, CURLOPT_NOBODY,true);
		//curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiefiles);	 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER ,false);
		$this->url=curl_exec($ch);
	}
 	public function getElements() {
 		$elements = phpQuery::newDocument($this->url);
 		$elements = $elements->attr($this->typeAttribute, $this->valueAttribute)->find($this->find);
 		foreach ($elements as $element) {
			$pqLink = pq($element); //pq делает объект phpQuery
			$text[] = $pqLink->html();
		}
		return $text;
 	}
}

$goods = new getContent;
$goods->setValues("https://www.olx.ua/list/q-passat/", "class", "marginright5 link linkWithHash detailsLink", "strong");
$goods->curl_content();
print_r($goods->getElements());

?>


